﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.Models
{
    public class Usuario
    {

        public int Id { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]  
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Apellido { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        [RegularExpression(@"[a-zA-Z0-9]+")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
      
        [MinLength(3)]          
        public string Password { get; set; }

        public List<Inscribirse> Inscribirse { get; set; }
        public List<ComentarioLeccion> ComentariosLeccion { get; set; }
    }
}
