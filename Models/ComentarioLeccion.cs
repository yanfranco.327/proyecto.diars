﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.Models
{
    public class ComentarioLeccion
    {
        public int Id { get; set; }

        public int ComentarioId { get; set; }
        public int LeccionId { get; set; }
        public int UsuarioId { get; set; }
        public string Detalle { get; set; }
        public DateTime Fecha { get; set; }

        public string UsuarioNombre { get; set; }
        public List<ComentarioLeccion> Comentarios { get; set; }
    }
}
