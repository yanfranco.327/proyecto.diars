﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.Models
{

    public class CursoProfesor
    {
        public int Id { get; set; }
        public int ProfesorId { get; set; } //PRimero el nombre

        public int CursoId { get; set; }

        
        public Profesor Profesor { get; set; }


    }
}
