﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.Models
{
    public class Leccion
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Contenido { get; set; }
        public int CapituloId { get; set; }
        public string Tipo { get; set; }

        public List<ComentarioLeccion> Comentarios { get; set; }
        // public Capitulo Capitulo { get; set; }

    }
}
