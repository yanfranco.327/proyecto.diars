﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.Models
{
    public class Curso
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Caratula { get; set; }
        public double Precio { get; set; }
        public List<CursoProfesor> CursoProfesores { get; set; }
        public List<Capitulo> Capitulos { get; set; }
        public List<Inscribirse> Inscribirse { get; set; }


    }
}
