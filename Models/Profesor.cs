﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.Models
{
    public class Profesor
    {
        
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Celular { get; set; }  //Solo mostrar , no se hacer operaciones(-+/)
        public string Profesion { get; set; }
        public string Imagen { get; set; }

    }
}
