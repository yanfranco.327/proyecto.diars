﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.Models
{
    public class ClaseTomada
    {

        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdLeccion { get; set; }

        public Usuario Usuario { get; set; }
        public Leccion Leccion { get; set; }


    }
}
