﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;

namespace MiProyecto.DB.Maps
{
    public class ClasesTomadaMaps : IEntityTypeConfiguration<ClaseTomada>
    {
        public void Configure(EntityTypeBuilder<ClaseTomada> builder)
        {
            builder.ToTable("ClasesTomadas");
            builder.HasKey(o=> o.Id);

            builder.HasOne(o => o.Usuario).WithMany().HasForeignKey(o => o.IdUser);
            builder.HasOne(o => o.Leccion).WithMany().HasForeignKey(o => o.IdLeccion);

            // builder.HasMany(o => o.Usuario)  //Curso-capitulos -lecciones.
            //.WithOne()
            //.HasForeignKey(o => o.);

        }
    }
}
