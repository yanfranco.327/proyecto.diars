﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.DB.Maps
{
    public class CursoMaps : IEntityTypeConfiguration<Curso>
    {
        public void Configure(EntityTypeBuilder<Curso> builder)
        {
            builder.ToTable("Cursos");
            builder.HasKey(o => o.Id);
           
            builder.HasMany(o => o.Capitulos)
                .WithOne()
                .HasForeignKey(o => o.CursoId);

            builder.HasMany(o => o.CursoProfesores)
                .WithOne()
                .HasForeignKey(o => o.CursoId);


        }
    }
}
