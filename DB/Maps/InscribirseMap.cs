﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;

namespace MiProyecto.DB.Maps
{
    public class InscribirseMap : IEntityTypeConfiguration<Inscribirse>
    {
        public void Configure(EntityTypeBuilder<Inscribirse> builder)
        {

            builder.ToTable("Inscribirse");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Usuario)
                .WithMany(o => o.Inscribirse)
                .HasForeignKey(o => o.IdUser);

            builder.HasOne(o => o.Curso)
                .WithMany(o => o.Inscribirse)
                .HasForeignKey(o => o.IdCurso);
        }
    }
}
        
 