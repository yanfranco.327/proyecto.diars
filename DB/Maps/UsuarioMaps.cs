﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;

namespace MiProyecto.DB.Maps
{
    public class UsuarioMaps : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuarios");
            builder.HasKey(o => o.Id);
            builder.HasMany(o => o.ComentariosLeccion)
                .WithOne()
                .HasForeignKey(o => o.UsuarioId);
        }
    }
}
