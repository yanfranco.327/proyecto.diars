﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;

namespace MiProyecto.DB.Maps
{
    public class CursoProfesorMaps : IEntityTypeConfiguration<CursoProfesor>
    {
        public void Configure(EntityTypeBuilder<CursoProfesor> builder)
        {
            builder.ToTable("CursoProfesores");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Profesor)
                .WithMany()
                .HasForeignKey(o => o.ProfesorId);
            
        }
    }
}
