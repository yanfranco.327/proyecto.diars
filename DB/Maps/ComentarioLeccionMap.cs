﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.DB.Maps
{
    public class ComentarioLeccionMap : IEntityTypeConfiguration<ComentarioLeccion>
    {
        public void Configure(EntityTypeBuilder<ComentarioLeccion> builder)
        {

            builder.ToTable("ComentarioLeccion");
            builder.HasKey(o => o.Id);

            builder.HasMany(o => o.Comentarios).WithOne().HasForeignKey(o=>o.ComentarioId);
        }
    }
}