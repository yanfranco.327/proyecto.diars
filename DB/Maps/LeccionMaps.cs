﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;

namespace MiProyecto.DB.Maps
{
    public class LeccionMaps : IEntityTypeConfiguration<Leccion>
    {
        public void Configure(EntityTypeBuilder<Leccion> builder)
        {

            builder.ToTable("Lecciones");
            builder.HasKey(o => o.Id);

            builder.HasMany(o => o.Comentarios)
                .WithOne()
                .HasForeignKey(o => o.LeccionId);

        }
    }
}
