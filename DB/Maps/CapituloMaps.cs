﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;


namespace MiProyecto.DB.Maps
{
    public class CapituloMaps : IEntityTypeConfiguration<Capitulo>
    {
        public void Configure(EntityTypeBuilder<Capitulo> builder)
        {
            builder.ToTable("Capitulos");
            builder.HasKey(o => o.Id);
            builder.HasMany(o => o.Lecciones)
                .WithOne()
                .HasForeignKey(o => o.CapituloId);


        }
    }
}
