﻿using Microsoft.EntityFrameworkCore;
using MiProyecto.DB.Maps;
using MiProyecto.Models;

namespace MiProyecto.DB
{
    public class AppProyectoContext : DbContext
    {
        public DbSet<Profesor> Profesores { get; set; }
        public DbSet<Capitulo> Capitulos { get; set; }
        public DbSet<Leccion> Lecciones { get; set; }
        public DbSet<Curso> Cursos { get; set; }
        public DbSet<Adjunto> Adjuntos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Inscribirse> Inscribirses { get; set; }
        public DbSet<CursoProfesor> CursoProfesores { get; set; }
        public DbSet<ClaseTomada> ClaseTomadas { get; set; }

        public DbSet<ComentarioLeccion> ComentariosLeccion { get; set; }


        public AppProyectoContext(DbContextOptions<AppProyectoContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ProfesorMaps());
            modelBuilder.ApplyConfiguration(new CursoMaps());
            modelBuilder.ApplyConfiguration(new CapituloMaps());
            modelBuilder.ApplyConfiguration(new LeccionMaps());
            modelBuilder.ApplyConfiguration(new AdjuntoMaps());
            modelBuilder.ApplyConfiguration(new CursoProfesorMaps());
            modelBuilder.ApplyConfiguration(new UsuarioMaps());
            modelBuilder.ApplyConfiguration(new InscribirseMap());
            modelBuilder.ApplyConfiguration(new ClasesTomadaMaps());
            modelBuilder.ApplyConfiguration(new ComentarioLeccionMap());


        }
    }
}
