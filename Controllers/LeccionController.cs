﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MiProyecto.DB;
using MiProyecto.Models;
using Microsoft.EntityFrameworkCore;

namespace MiProyecto.Controllers
{
    public class LeccionController : Controller
    {
        private AppProyectoContext context;

        public LeccionController(AppProyectoContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Detalle(int id, string opcion)
        {
            if (opcion != null) {
                id = CambiarLeccion(id,opcion);
            }
            var usuario = GetUsuario();

            ViewBag.leido = context.ClaseTomadas
                .Where(x => x.IdLeccion == id && x.IdUser == usuario.Id)
                .FirstOrDefault();
            ViewBag.Comentarios = context.ComentariosLeccion.Include(o=>o.Comentarios).Where(o => o.LeccionId == id && o.ComentarioId==0).ToList();
            
            ViewBag.usuario = GetUsuario();
            var leccion = context.Lecciones.Where(o => o.Id== id).FirstOrDefault();
            
            ViewBag.Capitulo = context.Capitulos.Where(o => o.Id ==leccion.CapituloId).FirstOrDefault();
            ViewBag.Lecciones = context.Lecciones.Where(o => o.CapituloId == leccion.CapituloId).ToList();
            return View(leccion);
        }

        [Authorize]
        public IActionResult MarcarComoLeido(int id)
        {
            var usuario = GetUsuario();
            var leccion = context.Lecciones.Where(o => o.Id == id).FirstOrDefault();

            var claseTomada = new ClaseTomada();
            claseTomada.IdLeccion = leccion.Id;
            claseTomada.IdUser = usuario.Id;
            context.ClaseTomadas.Add(claseTomada);
            context.SaveChanges();

            return RedirectToAction("Detalle", new { id = id });
        }

        [Authorize]
        public IActionResult MarcarComoNoLeido(int id)
        {
            var usuario = GetUsuario();
            var claseTomada = context.ClaseTomadas
                .Where(x => x.IdLeccion == id && x.IdUser == usuario.Id)
                .FirstOrDefault();

            context.ClaseTomadas.Remove(claseTomada);
            context.SaveChanges();

            return RedirectToAction("Detalle", new { id = id });
        }
        public ActionResult Comentar(ComentarioLeccion comentario)
        {
            var cursos = context.Cursos.ToList();

            if (ModelState.IsValid)
            {
                var usuario = GetUsuario();
                comentario.UsuarioId = usuario.Id;
                comentario.UsuarioNombre = usuario.Username;
                comentario.Fecha = DateTime.Now;
                context.ComentariosLeccion.Add(comentario);
                context.SaveChanges();
                return RedirectToAction("Detalle", new { id = comentario.LeccionId });
            }
            return RedirectToAction("Detalle", new { id = comentario.LeccionId });
        }
        private Usuario GetUsuario()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            if (claim != null)
            {
                var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
                return user;
            }

            return null;
        }
        
        private int CambiarLeccion(int id, string opcion) {
            var leccion = context.Lecciones.Where(o=>o.Id == id).FirstOrDefault();
            int newId = 0;
            int index = 0;
            var lecciones = context.Lecciones.Where(o=>o.CapituloId==leccion.CapituloId).ToList();
            
            for (int i=0;i<lecciones.Count ; i++) {
                if (lecciones[i].Id==leccion.Id) {
                    index = i;
                }
            }
            if (opcion=="siguiente") {
                return lecciones[index+1].Id;
            }

            return lecciones[index - 1].Id;
        }


    }
}
