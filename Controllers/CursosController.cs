﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MiProyecto.DB;
using MiProyecto.Models;

namespace MiProyecto.Controllers
{
    // La clase cursoDetalle solo tiene un Curso y una lista de CursoProfesor
    // Dentro de cursoProfesor yo puedo acceder a cada uno de los profesores
    public class CursoDetalle
    {
        public Curso Curso { get; set; }
        public bool Matriculado { get; set; }
        public List<Profesor> Profesores { get; set; }
    }
    
    public class CursosController : Controller
    {
        private AppProyectoContext context;

        public CursosController(AppProyectoContext context)
        {
            this.context = context;
        }

        
        public IActionResult Detalle(int id)
        {
            ViewBag.usuario = GetUsuario();
            ViewBag.progreso = ObtenerProgreso(id);
            // Obtengo el curso por su ID
            var curso = context.Cursos.FirstOrDefault(o => o.Id == id);

            // Obtengo todos los cursosDetalle que coincidan con el CursoID
            var cursoProfesores = context.CursoProfesores.Where(o => o.CursoId == curso.Id).Include(o => o.Profesor).ToList(); //Envia como null

            var profesores = new List<Profesor>();

            foreach (var cursoProfesor in cursoProfesores)
            {
                var profesor = context.Profesores.FirstOrDefault(o => o.Id == cursoProfesor.ProfesorId);
                profesores.Add(profesor);
            }

            // Creo un objeto de la clase CursoDetalle para enviar por Model los datos
            var cursoDetalle = new CursoDetalle
            {
                Curso = curso,
                Profesores = profesores, Matriculado = false
            };

            //----------
            var usuario = GetUsuario();
            if(usuario != null)
            {
                var matriculado = context.Inscribirses.Where(o => o.IdCurso == curso.Id && o.IdUser == GetUsuario().Id).FirstOrDefault();

                if (matriculado != null)
                {
                    cursoDetalle.Matriculado = true;
                    ViewBag.MICU = cursoDetalle.Matriculado;
                }
                else
                    ViewBag.MICU = false;
            }
            else
                ViewBag.MICU = false;

            return View(cursoDetalle);
        }
        [HttpGet]
        public ActionResult Matricular(int id)
        {
            var misCursos = context.Inscribirses.ToList();

            foreach (var item in misCursos)
            {
                if (item.IdCurso == id && item.IdUser == GetUsuario().Id)
                {
                    TempData["Error"] = "Curso matriculado";
                    ModelState.AddModelError("Error","Matriculado");
                }
            }
            if (ModelState.IsValid)
            {
                var matricula = new Inscribirse();

                matricula.IdCurso = id;
                matricula.IdUser = GetUsuario().Id;
                context.Inscribirses.Add(matricula);
                context.SaveChanges();
                return RedirectToAction("CatalogoCursos", "Home");
            }
            return RedirectToAction("CatalogoCursos", "Home");
        }

        private Usuario GetUsuario()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            if (claim != null)
            {
                var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
                return user;
            }

            return null;
        }

        [Authorize]
        public IActionResult MisCursos()
        {

            ViewBag.usuario = GetUsuario();
            var cursos = context.Inscribirses.Include(o=>o.Curso)
                .ThenInclude(o=>o.CursoProfesores)
                .ThenInclude(o=>o.Profesor)
                .Where(o => o.IdUser == GetUsuario().Id).ToList();

            return View(cursos);
        }

        
        private int ObtenerProgreso(int idCurso) {
            var capitulos = context.Capitulos.Where(o => o.CursoId == idCurso).Include(o => o.Lecciones).ToList();
            var progreso = 0;
            if (capitulos.Count == 0)
            {
                return 0;
            }
            foreach (var capitulo in capitulos)
            {
                progreso += ObtenerProgresoCapitulo(capitulo);
            }
            return progreso/capitulos.Count;
        }
        private int ObtenerProgresoCapitulo(Capitulo capitulo)
        {
            var usuario = GetUsuario();
            var lecciones = context.Lecciones.Where(o => o.CapituloId == capitulo.Id).ToList();
            var leccionesCompletadas = 0;
            var leccionCompletada = new ClaseTomada();
            if (lecciones.Count() == 0)
            {
                return 0;
            }
            foreach (var leccion in lecciones)
            {
                leccionCompletada = context.ClaseTomadas.
                    Where(o => o.IdLeccion == leccion.Id && o.IdUser == usuario.Id).
                    FirstOrDefault();
                if (leccionCompletada != null)
                {
                    leccionesCompletadas++;
                }
            }
            return (leccionesCompletadas * 100 / lecciones.Count());
        }

    }
}
