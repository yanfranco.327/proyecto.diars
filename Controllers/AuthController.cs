﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MiProyecto.DB.Maps;
using MiProyecto.DB;
using MiProyecto.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Security.Cryptography;



namespace MiProyecto.Controllers
{
    public class AuthController : Controller
    {
        private AppProyectoContext context;
        private IConfiguration configuration;
        public AuthController(AppProyectoContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var hash = CreateHash(password);
            var user = context.Usuarios
                .FirstOrDefault(o => o.Username == username && o.Password == hash);

            if (user == null)
            {
                TempData["AuthMessage"] = "Usuario o Password incorrecto";
                HttpContext.Response.StatusCode = 400;

                return View("Login");
            }

            // Autenticar
            var claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.Username),
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            HttpContext.SignInAsync(claimsPrincipal);
            if (user.Username == "admin") {
                return RedirectToAction("Index", "Admin");
            }
            return RedirectToAction("CatalogoCursos", "Home");
        }

     
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }

        [HttpGet]
        public string Create(string password)
        {
            return CreateHash(password);
        }

        private string CreateHash(string input)
        {
            input += configuration.GetValue<string>("Key");
            var sha = SHA512.Create();

            var bytes = Encoding.Default.GetBytes(input);
            var hash = sha.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        }


        [HttpGet]
        public ActionResult Registrar()
        {
            return View();
        }
      
        [HttpPost]
        public ActionResult Registrar(Usuario usuario, string PasswordConf)
        {
            var usuarios = context.Usuarios.ToList();
            foreach (var item in usuarios)
            {
                if (item.Email == usuario.Email)
                    ModelState.AddModelError("Email", "Este Email ya esta registrado !");
                if (item.Username == usuario.Username)
                    ModelState.AddModelError("Username", "Este usuario ya esta registrado !");
            }

            if (usuario.Password != PasswordConf) // <-- para convalidar contraseña y confirmacion de contraseña
                   ModelState.AddModelError("passwordNew", "Las contraseñas no coinciden !");
           
                if (ModelState.IsValid)
                {
                usuario.Email = usuario.Email;
                usuario.Password = CreateHash(PasswordConf);
                    context.Usuarios.Add(usuario);
                    context.SaveChanges();
                    return RedirectToAction("Login");
                }
                return View("Registrar", usuario);
            }
        }
    }
