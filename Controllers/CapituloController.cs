﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MiProyecto.DB;
using MiProyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MiProyecto.Controllers
{
    public class CapituloController : Controller
    {
        private AppProyectoContext context;

        public CapituloController(AppProyectoContext context)
        {
            this.context = context;
        }
        public ActionResult Index(int idCurso)
        {

            ViewBag.usuario = GetUsuario();
            ViewBag.idCurso = idCurso;
            var capitulos = context.Capitulos.Where(o => o.CursoId == idCurso).Include(o => o.Lecciones).ToList();
            var porcentajes = new List<int>();
            foreach (var capitulo in capitulos) {
                porcentajes.Add(ObtenerProgreso(capitulo));
            }
            ViewBag.porcentajes=porcentajes;
            ViewBag.cantidad = capitulos.Count;
            return View(capitulos);
        }


        public ActionResult Details(int id)
        {
            return View();
        }


        public ActionResult Create()
        {
            return View();
        }
        private Usuario GetUsuario()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            if (claim != null)
            {
                var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
                return user;
            }

            return null;
        }
        

        private int ObtenerProgreso(Capitulo capitulo){
            var usuario = GetUsuario();
            var lecciones = context.Lecciones.Where(o=>o.CapituloId ==capitulo.Id).ToList();
            var leccionesCompletadas = 0;
            var leccionCompletada = new ClaseTomada();
            if (lecciones.Count() == 0)
            {
                return 0;
            }
            foreach (var leccion in lecciones) {
                leccionCompletada = context.ClaseTomadas.
                    Where(o=>o.IdLeccion == leccion.Id && o.IdUser==usuario.Id).
                    FirstOrDefault();
                if (leccionCompletada!=null) {
                    leccionesCompletadas++;
                }
            }
            return (leccionesCompletadas * 100 / lecciones.Count());
        }


    }
}
