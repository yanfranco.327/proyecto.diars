﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using MiProyecto.DB;
using MiProyecto.Models;

namespace MiProyecto.Controllers
{
    public class HomeController : Controller
    {
        private AppProyectoContext context;
        public HomeController(AppProyectoContext context)
        {
            this.context = context;
        }


        
        public IActionResult Index()
        {
            ViewBag.cursos = context.Cursos.Include(o => o.CursoProfesores).ThenInclude(o => o.Profesor).Take(4).ToList();
            ViewBag.profesores = context.Profesores.Take(4).ToList();
            ViewBag.usuario = GetUsuario();
            return View();
        }
        public IActionResult CatalogoCursos()
        {
            var cursos = context.Cursos.Include(o => o.CursoProfesores).ThenInclude(o => o.Profesor).ToList();
            //  var cursos = context.Cursos.ToList();
            
            ViewBag.usuario = GetUsuario();
            return View(cursos);

        }
        private Usuario GetUsuario()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            if (claim != null)
            {
                var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
                return user;
            }

            return null;
        }
        
    }
}
