﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MiProyecto.DB;
using MiProyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace MiProyecto.Controllers
{
    public class AdminController : Controller
    {
        private AppProyectoContext context;
        private IWebHostEnvironment hosting;

        public AdminController(AppProyectoContext context, IWebHostEnvironment hosting)
        {
            this.hosting = hosting;
            this.context = context;
        }

        // GET: AdminController
        public ActionResult Index()
        {
            return View();
        }

        //CRUD CURSOS
        public ActionResult ListarCursos()
        {
            var cursos = context.Cursos.Include(o => o.CursoProfesores).ThenInclude(o => o.Profesor).ToList();

            return View(cursos);
        }
        [HttpGet]
        public ActionResult CrearCurso()
        {
            ViewBag.profesores = context.Profesores.ToList();
            return View();
        }

        [HttpPost]
        public ActionResult CrearCurso(Curso curso, IFormFile files, int profesorId)
        {            
            if (ModelState.IsValid)
            {
                curso.Caratula = SaveFileImage(files);

                context.Cursos.Add(curso);
                context.SaveChanges();
                var cursos = context.Cursos.ToList();
                var cursofinal = cursos.Last();
                context.CursoProfesores.Add(new CursoProfesor() { CursoId = cursofinal.Id, ProfesorId = profesorId });
                context.SaveChanges();
                return RedirectToAction("ListarCursos");
            }
            return RedirectToAction("CrearCurso");
            
        }
        [HttpGet]
        public ActionResult EditarCurso(int id)
        {
            var curso = context.Cursos.Where(o => o.Id == id).FirstOrDefault();
            ViewBag.profesores = context.Profesores.ToList();
            return View(curso);
        }

        [HttpPost]
        public ActionResult EditarCurso(Curso curso, IFormFile files, int profesorId)
        {
            if (ModelState.IsValid)
            {
                var curso_db = context.Cursos.Find(curso.Id);
                curso_db.Titulo = curso.Titulo;
                curso_db.Descripcion = curso.Descripcion;
                curso_db.Precio = curso.Precio;

                if (profesorId != 0)
                {
                    var cursoProfesor = context.CursoProfesores.Where(o => o.CursoId == curso.Id).FirstOrDefault();
                    context.CursoProfesores.Remove(cursoProfesor);
                    context.CursoProfesores.Add(new CursoProfesor() { CursoId = curso.Id, ProfesorId = profesorId });

                    context.SaveChanges();
                    
                }
                if (files!=null) {
                    var aux = Path.Combine(hosting.WebRootPath, curso_db.Caratula.Remove(0, 1));

                    if (System.IO.File.Exists(aux))
                    {
                        System.IO.File.Delete(aux);
                    }
                    curso_db.Caratula = SaveFileImage(files);

                }



                context.SaveChanges();
                return RedirectToAction("ListarCursos");
            }
            return RedirectToAction("CrearCurso");

        }
        public ActionResult DetalleCurso(int id)
        {
            var curso = context.Cursos.Where(o => o.Id == id).Include(o=>o.Capitulos).FirstOrDefault();
            ViewBag.capitulos = curso.Capitulos.Count;
            
            return View(curso);
        }


        //Usuarios
        public ActionResult ListarUsuarios()
        {
            var usuarios = context.Usuarios.ToList();
            return View(usuarios);
        }
       
        public ActionResult DetalleUsuario(int id)
        {
            var usuario = context.Usuarios.Where(o=>o.Id==id).Include(o=>o.Inscribirse).ThenInclude(o=>o.Curso).FirstOrDefault();
            double gastado = 0;
            foreach (var item in usuario.Inscribirse) {
                gastado += item.Curso.Precio;
            
            }
            ViewBag.totalGastado = gastado;
            return View(usuario);
        }

        //CRUD PROFESORES
        public ActionResult ListarProfesores()
        {
            var profesores = context.Profesores.ToList();
            return View(profesores);
        }
        [HttpGet]
        public ActionResult CrearProfesor()
        {

            return View(new Profesor());
        }

        [HttpPost]
        public ActionResult CrearProfesor(Profesor profesor, IFormFile files)
        {
            if (ModelState.IsValid)
            {
                profesor.Imagen= SaveFileImage(files);

                context.Profesores.Add(profesor);
                context.SaveChanges();
                return RedirectToAction("ListarProfesores");
            }
            return View(profesor);

        }

        public ActionResult DetalleProfesor(int id)
        {
            var profesor = context.Profesores.Where(o=>o.Id==id).FirstOrDefault();
            return View(profesor);
        }


        [HttpGet]
        public ActionResult EditarProfesor(int id)
        {
            var profesor = context.Profesores.Where(o=>o.Id == id).FirstOrDefault();
            return View(profesor);
        }

        [HttpPost]
        public ActionResult EditarProfesor(Profesor profesor, IFormFile files)
        {
            if (ModelState.IsValid)
            {
                var profesor_db = context.Profesores.Find(profesor.Id);
                profesor_db.Nombre = profesor.Nombre;
                profesor_db.Apellido = profesor.Apellido;
                profesor_db.Profesion = profesor.Profesion;
                profesor_db.Celular = profesor.Celular;
                if (files != null)
                {
                    var aux = Path.Combine(hosting.WebRootPath, profesor_db.Imagen.Remove(0, 1));

                    if (System.IO.File.Exists(aux))
                    {
                        System.IO.File.Delete(aux);
                    }
                    profesor_db.Imagen = SaveFileImage(files);

                }

                context.SaveChanges();
                return RedirectToAction("ListarProfesores");
            }
            return View(profesor);

        }

        //CRUD CAPITULOS
        public ActionResult ListarCapitulos(int id)
        {
            
            var curso = context.Cursos.Where(o=> o.Id == id).FirstOrDefault();
            ViewBag.curso = curso;
            var capitulos = context.Capitulos.Where(o => o.CursoId == id).Include(o=>o.Lecciones).ToList();
            return View(capitulos);
        }
        [HttpGet]
        public ActionResult CrearCapitulo(int idCurso)
        {
            var curso = context.Cursos.Where(o => o.Id == idCurso).FirstOrDefault();
            ViewBag.curso = curso;

            return View();
        }
        [HttpPost]
        public ActionResult CrearCapitulo(Capitulo capitulo)
        {
            if (ModelState.IsValid)
            {

                context.Capitulos.Add(capitulo);
                context.SaveChanges();
                return RedirectToAction("ListarCapitulos", new { id = capitulo.CursoId });
            }
            return RedirectToAction("CrearCapitulo");

        }
        [HttpGet]
        public ActionResult EditarCapitulo(int id)
        {
            var capitulo = context.Capitulos.Where(o=> o.Id==id).FirstOrDefault();
            var curso = context.Cursos.Where(o => o.Id == capitulo.CursoId).FirstOrDefault();
            ViewBag.curso = curso;

            return View(capitulo);
        }
        [HttpPost]
        public ActionResult EditarCapitulo(Capitulo capitulo)
        {
            if (ModelState.IsValid)
            {
                var capitulo_db = context.Capitulos.Find(capitulo.Id);
                capitulo_db.Titulo = capitulo.Titulo;
                capitulo_db.Descripcion = capitulo.Descripcion;
                context.SaveChanges();
                return RedirectToAction("ListarCapitulos", new { id = capitulo.CursoId });
            }
            return RedirectToAction("CrearCapitulo");

        }

        public ActionResult EliminarCapitulo(int id)
        {
            var capitulo = context.Capitulos.Find(id);
            var cursoId = capitulo.CursoId;
            var lecciones = context.Lecciones.Where(o=>o.CapituloId == capitulo.Id).ToList();
            foreach (var item in lecciones) {
                if (item.Tipo == "pdf")
                {
                    var aux = Path.Combine(hosting.WebRootPath, item.Contenido.Remove(0, 1));

                    if (System.IO.File.Exists(aux))
                    {
                        System.IO.File.Delete(aux);
                    }
                }
                var clasesTomadas = context.ClaseTomadas.Where(o => o.IdLeccion == item.Id).ToList();
                var comentarios = context.ComentariosLeccion.Where(o => o.LeccionId == item.Id).ToList();
                foreach (var clases in clasesTomadas)
                {
                    context.ClaseTomadas.Remove(clases);

                    context.SaveChanges();
                }
                foreach (var comentario in comentarios)
                {
                    context.ComentariosLeccion.Remove(comentario);

                    context.SaveChanges();
                }
                context.Lecciones.Remove(item);
                context.SaveChanges();


            }
            context.Capitulos.Remove(capitulo);
            context.SaveChanges();

            return RedirectToAction("ListarCapitulos", new { id = cursoId });
        }



        //CRUD LECCIONES
        public ActionResult DetalleLeccion(int id)
        {
            var leccion = context.Lecciones.Where(o => o.Id == id).FirstOrDefault();
            ViewBag.capitulo = context.Capitulos.Where(o=>o.Id == leccion.CapituloId).FirstOrDefault();

            return View(leccion);
        }
        [HttpGet]
        public ActionResult CrearLeccion(int idCapitulo)
        {
            var capitulo = context.Capitulos.Where(o => o.Id == idCapitulo).FirstOrDefault();
            ViewBag.capitulo = capitulo;

            return View();
        }
        [HttpPost]
        public ActionResult CrearLeccion(Leccion leccion, IFormFile files)
        {
            if (ModelState.IsValid)
            {
                if (files != null)
                {
                    leccion.Contenido = SaveFilePdf(files);
                }
                else {
                    leccion.Contenido = leccion.Contenido.Replace("https://www.youtube.com/watch?v=", "https://www.youtube.com/embed/");
                }
                context.Lecciones.Add(leccion);
                context.SaveChanges();
                var capitulo = context.Capitulos.Where(o => o.Id == leccion.CapituloId).FirstOrDefault(); 
                return RedirectToAction("ListarCapitulos", new { id = capitulo.CursoId });
            }
            return RedirectToAction("CrearLeccion", new { idCapitulo = leccion.CapituloId });

        }
        [HttpGet]
        public ActionResult EditarLeccion(int id)
        {
            var leccion = context.Lecciones.Where(o => o.Id == id).FirstOrDefault();
            var capitulo = context.Capitulos.Where(o => o.Id == leccion.CapituloId).FirstOrDefault();
            ViewBag.capitulo = capitulo;

            return View(leccion);
        }
        [HttpPost]
        public ActionResult EditarLeccion(Leccion leccion, IFormFile files)
        {
            if (ModelState.IsValid)
            {
                var leccion_db = context.Lecciones.Find(leccion.Id);
                leccion_db.Titulo = leccion.Titulo;
                leccion_db.Descripcion = leccion.Descripcion;
                if (leccion.Tipo !="No") {
                    if (leccion_db.Tipo == "pdf")
                    {
                        var aux = Path.Combine(hosting.WebRootPath, leccion_db.Contenido.Remove(0, 1));

                        if (System.IO.File.Exists(aux))
                        {
                            System.IO.File.Delete(aux);
                        }
                    }

                    leccion_db.Tipo = leccion.Tipo;
                    if (files != null)
                    {
                        leccion_db.Contenido = SaveFilePdf(files);
                    }
                    else
                    {
                        leccion_db.Contenido = leccion.Contenido.Replace("https://www.youtube.com/watch?v=", "https://www.youtube.com/embed/");
                    }
                }
                context.SaveChanges();
                var capitulo = context.Capitulos.Where(o => o.Id == leccion_db.CapituloId).FirstOrDefault();
                return RedirectToAction("ListarCapitulos", new { id = capitulo.CursoId });
            }
            return RedirectToAction("CrearLeccion", new { idCapitulo = leccion.CapituloId });

        }
        public ActionResult EliminarLeccion(int id)
        {

            var leccion = context.Lecciones.Find(id);
            if (leccion.Tipo=="pdf") {
                var aux = Path.Combine(hosting.WebRootPath, leccion.Contenido.Remove(0, 1));
                
                if (System.IO.File.Exists(aux))
                {
                    System.IO.File.Delete(aux);
                }
            }
            var clasesTomadas= context.ClaseTomadas.Where(o=>o.IdLeccion==leccion.Id).ToList();
            var comentarios = context.ComentariosLeccion.Where(o=>o.LeccionId==leccion.Id).ToList();
            foreach (var item in clasesTomadas) {
                context.ClaseTomadas.Remove(item);

                context.SaveChanges();
            }
            foreach (var item in comentarios)
            {
                context.ComentariosLeccion.Remove(item);

                context.SaveChanges();
            }
            var capitulo = context.Capitulos.Where(o => o.Id == leccion.CapituloId).FirstOrDefault();
            context.Lecciones.Remove(leccion);

            context.SaveChanges();

            return RedirectToAction("ListarCapitulos", new { id = capitulo.CursoId });
        }

        private string SaveFilePdf(IFormFile file)
        {
            string relativePath = "";

            if (file.Length > 0 && file.ContentType == "application/pdf")
            {
                relativePath = Path.Combine("files", file.FileName);
                var filePath = Path.Combine(hosting.WebRootPath, relativePath);
                var stream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
            }

            return "/" + relativePath.Replace('\\', '/');
        }
        private string SaveFileImage(IFormFile file)
        {
            string relativePath = "";

            if (file.Length > 0 )
            {
                relativePath = Path.Combine("files", file.FileName);
                var filePath = Path.Combine(hosting.WebRootPath, relativePath);
                var stream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
            }

            return "/" + relativePath.Replace('\\', '/');
        }
    }
}
